package com.example.voteforyourfavorite

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tech.twentytwobits.recyclerviewexample.ClickListener

class AdapterCustom (var contex: Context, var items: ArrayList<Couple>, var clickListener: ClickListener): RecyclerView.Adapter<AdapterCustom.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(contex).inflate(R.layout.cardview_template, parent, false)
        return ViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        // Mapeo
        holder.nombre.text = item.nombre
        holder.foto.setImageResource(item.foto)
    }

    /////////
    class ViewHolder (var view: View, var clickListener: ClickListener): RecyclerView.ViewHolder(view), View.OnClickListener {
        override fun onClick(v: View?) {
            return clickListener.onClick(view, adapterPosition)
        }

        var nombre: TextView
        var foto: ImageView

        init {
            this.nombre = view.findViewById(R.id.textView_Nombre)
            this.foto = view.findViewById(R.id.imageView_Pareja)

            view.setOnClickListener(this)
        }

    }
}