package com.example.voteforyourfavorite.DB

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import androidx.core.content.contentValuesOf

class DBHelper (context: Context): SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VER) {
    override fun onCreate(db: SQLiteDatabase?) {
        //crear tabla
        val CREATE_TABLE_QUERY = ("CREATE TABLE $TABLE_NAME($COL_ID INTEGER PRIMARY KEY, $COL_NAME TEXT, $COL_VOTOS INTEGER)")
        db!!.execSQL(CREATE_TABLE_QUERY)

        // Tabla user
        val CREATE_TABLE_QUERY_USER = ("CREATE TABLE $TABLE_NAME_USER($COL_ID INTEGER PRIMARY KEY AUTOINCREMENT, $COL_USER TEXT, $COL_VOTACION INTEGER)")
        db!!.execSQL(CREATE_TABLE_QUERY_USER)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL(("DROP TABLE IF EXISTS $TABLE_NAME"))
        onCreate(db!!)

        db!!.execSQL(("DROP TABLE IF EXISTS $TABLE_NAME_USER"))
        onCreate(db!!)
    }

    val allPerson:List<Pareja>
        get() {
            val lstPerson = ArrayList<Pareja>()
            val selectQuery = "SELECT * FROM $TABLE_NAME"
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery, null)

            if (cursor.moveToFirst()){
                do {
                    val pareja = Pareja()
                    pareja.id = cursor.getInt(cursor.getColumnIndex(COL_ID))
                    pareja.votos = cursor.getInt(cursor.getColumnIndex(COL_VOTOS))
                    pareja.name = cursor.getString(cursor.getColumnIndex(COL_NAME))

                    lstPerson.add(pareja)
                }while (cursor.moveToNext())
            }

            db.close()
            return lstPerson
        }


    /*
    Agregar datos
     */

    fun addCouple(pareja: Pareja)
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID, pareja.id)
        values.put(COL_NAME, pareja.name)
        values.put(COL_VOTOS, pareja.votos)
        db.insert(TABLE_NAME, null, values)
        db.close()
    }


    /*
    ACTUALIZAR
     */

    fun updateCouple(pareja: Pareja): Int
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID, pareja.id)
        values.put(COL_NAME, pareja.name)
        values.put(COL_VOTOS, pareja.votos)
        db.insert(TABLE_NAME, null, values)
        return db.update(TABLE_NAME, values, "$COL_ID=?", arrayOf(pareja.id.toString()))
    }

    //TABLA USER

    val allUser:List<Usuario>
        get() {
            val lstUser = ArrayList<Usuario>()
            val selectQuery = "SELECT * FROM $TABLE_NAME_USER"
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery, null)

            if (cursor.moveToFirst()){
                do {
                    val user = Usuario()
                    user.id = cursor.getInt(cursor.getColumnIndex(COL_ID_USER))
                    user.user = cursor.getString(cursor.getColumnIndex(COL_USER))
                    user.votaciones = cursor.getInt(cursor.getColumnIndex(COL_VOTACION))

                    lstUser.add(user)
                }while (cursor.moveToNext())
            }

            db.close()
            return lstUser
        }


    /*
    Agregar datos
     */

    fun addUser(user: Usuario)
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_USER, user.id)
        values.put(COL_USER, user.user)
        values.put(COL_VOTACION, user.votaciones)
        db.insert(TABLE_NAME_USER, null, values)
        db.close()
    }


    /*
    ACTUALIZAR
     */

    fun updateUser(user: Usuario): Int
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_USER, user.id)
        values.put(COL_USER, user.user)
        values.put(COL_VOTACION, user.votaciones)
        db.insert(TABLE_NAME_USER, null, values)
        return db.update(TABLE_NAME_USER, values, "$COL_ID_USER=?", arrayOf(user.id.toString()))
    }


    companion object{
        private val DATABASE_VER = 2
        private val DATABASE_NAME = "COUPLE.dp"


        // TABLA Pareja

        private val TABLE_NAME = "COUPLEVOTE"
        private val COL_ID = "id"
        private val COL_NAME = "name"
        private val COL_VOTOS = "votos"

        //Tabla Usuario

        private val TABLE_NAME_USER = "USER"
        private val COL_ID_USER = "id"
        private val COL_USER = "user"
        private val COL_VOTACION = "votacion"
    }
}