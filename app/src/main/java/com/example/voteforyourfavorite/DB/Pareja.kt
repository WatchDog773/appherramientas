package com.example.voteforyourfavorite.DB

class Pareja {
    var id: Int = 0
    var name: String? = null
    var votos: Int = 0

    constructor()
    {

    }

    constructor(id: Int, name: String, votos: Int)
    {
        this.id = id
        this.name = name
        this.votos = votos
    }
}