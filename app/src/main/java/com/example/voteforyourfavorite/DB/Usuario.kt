package com.example.voteforyourfavorite.DB

class Usuario {
    var id: Int? = null
    var user: String? = null
    var votaciones: Int = 0

    constructor()
    {

    }

    constructor(id: Int?, user: String, votaciones: Int)
    {
        this.id = id
        this.user = user
        this.votaciones = votaciones
    }
}