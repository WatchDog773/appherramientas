package com.example.voteforyourfavorite

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.voteforyourfavorite.DB.DBHelper
import com.example.voteforyourfavorite.DB.Pareja
import kotlinx.android.synthetic.main.activity_ingresar_parejas.*
import kotlinx.android.synthetic.main.activity_iniciar_sesion.*

class IngresarParejas : AppCompatActivity() {

    private lateinit var db: DBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ingresar_parejas)

        db = DBHelper(this)

        buttonIngresarIngresar.isEnabled = contarDatos()
        //Toast.makeText(applicationContext, "${contarDatos()}", Toast.LENGTH_SHORT).show()

        buttonIngresarIngresar.setOnClickListener{
            ingresar()
            Toast.makeText(applicationContext, "Se ingreso", Toast.LENGTH_SHORT).show()
        }

    }

    private fun contarDatos(): Boolean
    {
        var nno = true
        val lst = db.allPerson
        var con = lst.size
        if(con > 0)
        {
            nno = false
        }

        return nno
    }

    fun ingresar()
    {
        val pareja1 = Pareja(
            121,
            "Humberto Mejia y Giselle Ventura",
            23
        )
        db.addCouple(pareja1)

        val pareja2 = Pareja(
            122,
            "Alejandro Lara y Daniela Zaragoza",
            12
        )
        db.addCouple(pareja2)

        val pareja3 = Pareja(
            123,
            "Walter White y Skyler Lopez",
            71
        )
        db.addCouple(pareja3)

        val pareja4 = Pareja(
            124,
            "Hank Schrader y Marie Brandt",
            10
        )
        db.addCouple(pareja4)

        val pareja5 = Pareja(
            125,
            "Olman Iscoa y Reina Diaz",
            50
        )
        db.addCouple(pareja5)

        val pareja6 = Pareja(
            126,
            "Adan Cardona y Maria Chavarria",
            72
        )
        db.addCouple(pareja6)

        val pareja7 = Pareja(
            127,
            "Cristian Gomez y Maria De los Angeles",
            23
        )
        db.addCouple(pareja7)

        val pareja8 = Pareja(
            128,
            "Jesus Portillo y Larissa Rodriguez",
            34
        )
        db.addCouple(pareja8)

        val pareja9 = Pareja(
            129,
            "Juan Perez y Melisa Andrade",
            54
        )
        db.addCouple(pareja9)

        val pareja10 = Pareja(
            130,
            "Jose Perez y Andrea Sosa",
            70
        )
        db.addCouple(pareja10)

        val pareja11 = Pareja(
            131,
            "Juan Lopez y Antonia Gonzales",
            27
        )
        db.addCouple(pareja11)

        val pareja12 = Pareja(
            132,
            "Agustin Laje y Andrea Pereira",
            65
        )
        db.addCouple(pareja12)

        val pareja13 = Pareja(
            133,
            "Andres Rodriguez y Juana Argueta",
            69
        )
        db.addCouple(pareja13)

        val pareja14 = Pareja(
            134,
            "Antonio Ruiz y Jimena Velaquez",
            45
        )
        db.addCouple(pareja14)

        val pareja15 = Pareja(
            135,
            "Manuel Cantarero y Faustina Dias",
            25
        )
        db.addCouple(pareja15)

        val pareja16 = Pareja(
            136,
            "Alberto Lopez y Maria Sosa",
            20
        )
        db.addCouple(pareja16)

        val pareja17 = Pareja(
            137,
            "Pedro Ordoñes y Isabel Morales",
            24
        )
        db.addCouple(pareja17)

        val pareja18 = Pareja(
            138,
            "Antonio Mejia y Ana Mejia",
            100
        )
        db.addCouple(pareja18)

        val pareja19 = Pareja(
            139,
            "Daniel Mejia y Marcela Acosta",
            32
        )
        db.addCouple(pareja19)

        val pareja20 = Pareja(
            140,
            "Hector Salamanca y Libia Dominguez",
            82
        )
        db.addCouple(pareja20)

    }
}
