package com.example.voteforyourfavorite

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.voteforyourfavorite.DB.DBHelper
import com.example.voteforyourfavorite.DB.Usuario
import kotlinx.android.synthetic.main.activity_iniciar_sesion.*

class IniciarSesion : AppCompatActivity() {

    private lateinit var db: DBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_iniciar_sesion)

        db = DBHelper(this)

//        buttonEntrar.isEnabled = !contarDatos()

        buttonAgregarDatos.setOnClickListener{
            val intent1 = Intent(this, IngresarParejas::class.java)
            startActivity(intent1)
        }

        buttonEntrar.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            val u = editTextCuenta.text.toString()
            val b = Bundle()
            val empty = ""

            if(contarDatos())
            {
                Toast.makeText(applicationContext, "Registre Parejas", Toast.LENGTH_SHORT).show()
            }
            else{
                if(!editTextCuenta.text.isNotEmpty() || editTextCuenta.text.isNullOrBlank())
                {
                    Toast.makeText(applicationContext, "Ingrese una cuenta", Toast.LENGTH_SHORT).show()
                }
                else{
                    if(selectUser(u)){
                        Toast.makeText(applicationContext, "Usuario Existente", Toast.LENGTH_SHORT).show()
                        b.putString("User", u)
                        intent.putExtras(b)
                        startActivity(intent)
                    }else
                    {
                        val user = Usuario(
                            null,
                            editTextCuenta.text.toString(),
                            0
                        )
                        db.addUser(user)


                        b.putString("User", editTextCuenta.text.toString())
                        intent.putExtras(b)
                        startActivity(intent)
                        Toast.makeText(applicationContext, "Se agrego usuario", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }


    }

    private fun selectUser(user: String): Boolean
    {
        var nno = false
        val lst = db.allUser
        lst.forEach{
            if(it.user == user)
            {
                nno = true
            }
        }

        return nno
    }


    private fun contarDatos(): Boolean
    {
        var nno = true
        val lst = db.allPerson
        var con = lst.size
        if(con > 0)
        {
            nno = false
        }

        return nno
    }

}
