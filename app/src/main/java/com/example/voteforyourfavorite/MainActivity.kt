package com.example.voteforyourfavorite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import tech.twentytwobits.recyclerviewexample.ClickListener

class MainActivity : AppCompatActivity() {

    private lateinit var layoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bundle = intent.extras
        val user = bundle!!.getString("User")

        // Adaptador source

        val parejas = ArrayList<Couple>()

        parejas.add(Couple("Humberto Mejia y Giselle Ventura", R.drawable.couple_1))
        parejas.add(Couple("Alejandro Lara y Daniela Zaragoza", R.drawable.couple_2))
        parejas.add(Couple("Walter White y Skyler Lopez", R.drawable.couple_3))
        parejas.add(Couple("Hank Schrader y Marie Brandt", R.drawable.couple_4))
        parejas.add(Couple("Olman Iscoa y Reina Diaz", R.drawable.couple_5))
        parejas.add(Couple("Adan Cardona y Maria Chavarria", R.drawable.couple_6))
        parejas.add(Couple("Cristian Gomez y Maria De los Angeles", R.drawable.couple_7))
        parejas.add(Couple("Jesus Portillo y Larissa Rodriguez", R.drawable.couple_8))
        parejas.add(Couple("Juan Perez y Melisa Andrade", R.drawable.couple_9))
        parejas.add(Couple("Jose Perez y Andrea Sosa", R.drawable.couple_10))
        parejas.add(Couple("Juan Lopez y Antonia Gonzales", R.drawable.couple_11))
        parejas.add(Couple("Agustin Laje y Andrea Pereira", R.drawable.couple_12))
        parejas.add(Couple("Andres Rodriguez y Juana Argueta", R.drawable.couple_13))
        parejas.add(Couple("Antonio Ruiz y Jimena Velaquez", R.drawable.couple_14))
        parejas.add(Couple("Manuel Cantarero y Faustina Dias", R.drawable.couple_15))
        parejas.add(Couple("Alberto Lopez y Maria Sosa", R.drawable.couple_16))
        parejas.add(Couple("Pedro Ordoñes y Isabel Morales", R.drawable.couple_17))
        parejas.add(Couple("Antonio Mejia y Ana Mejia", R.drawable.couple_18))
        parejas.add(Couple("Daniel Mejia y Marcela Acosta", R.drawable.couple_19))
        parejas.add(Couple("Hector Salamanca y Libia Dominguez", R.drawable.couple_20))

        layoutManager = GridLayoutManager(this, 2)

        val adapter = AdapterCustom(this, parejas, object: ClickListener{
            override fun onClick(view: View, index: Int) {
                var dato: String = parejas[index].nombre
                //Toast.makeText(applicationContext, "$user", Toast.LENGTH_SHORT).show()
                val intent = Intent (this@MainActivity, VotacionPareja::class.java)
                val b = Bundle()
                val c = Bundle()
                b.putString("Nombre", dato)
                c.putString("User1", user)
                intent.putExtras(b)
                intent.putExtras(c)
                startActivity(intent)

            }
        })

        recyclerLayout_parejas.setHasFixedSize(true)
        recyclerLayout_parejas.layoutManager = layoutManager
        recyclerLayout_parejas.adapter = adapter

    }
}
