package com.example.voteforyourfavorite

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.voteforyourfavorite.DB.DBHelper
import com.example.voteforyourfavorite.DB.Pareja
import com.example.voteforyourfavorite.DB.Usuario
import kotlinx.android.synthetic.main.activity_votacion_pareja.*

class VotacionPareja : AppCompatActivity() {


    private lateinit var db: DBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_votacion_pareja)

        db = DBHelper(this)

        val bundle = intent.extras
        val nombre = bundle!!.getString("Nombre")
        val user = bundle!!.getString("User1")

        val nombrePareja = selectNombre(nombre.toString())
        var votosIniciales = selectVotos(nombre.toString())
        var porcentajeInicial = calcularPorcentaje()
        val idPareja = selectId(nombre.toString())

        when(idPareja.toInt()){

            121 -> imageViewFotoPareja.setImageResource(R.drawable.couple_1)

            122 -> imageViewFotoPareja.setImageResource(R.drawable.couple_2)

            123 -> imageViewFotoPareja.setImageResource(R.drawable.couple_3)

            124 -> imageViewFotoPareja.setImageResource(R.drawable.couple_4)

            125 -> imageViewFotoPareja.setImageResource(R.drawable.couple_5)

            126 -> imageViewFotoPareja.setImageResource(R.drawable.couple_6)

            127 -> imageViewFotoPareja.setImageResource(R.drawable.couple_7)

            128 -> imageViewFotoPareja.setImageResource(R.drawable.couple_8)

            129 -> imageViewFotoPareja.setImageResource(R.drawable.couple_9)

            130 -> imageViewFotoPareja.setImageResource(R.drawable.couple_10)

            131 -> imageViewFotoPareja.setImageResource(R.drawable.couple_11)

            132 -> imageViewFotoPareja.setImageResource(R.drawable.couple_12)

            133 -> imageViewFotoPareja.setImageResource(R.drawable.couple_13)

            134 -> imageViewFotoPareja.setImageResource(R.drawable.couple_14)

            135 -> imageViewFotoPareja.setImageResource(R.drawable.couple_15)

            136 -> imageViewFotoPareja.setImageResource(R.drawable.couple_16)

            137 -> imageViewFotoPareja.setImageResource(R.drawable.couple_17)

            138 -> imageViewFotoPareja.setImageResource(R.drawable.couple_18)

            139 -> imageViewFotoPareja.setImageResource(R.drawable.couple_19)

            140 -> imageViewFotoPareja.setImageResource(R.drawable.couple_20)
        }


        textViewParejaVotar.text = nombrePareja
        textViewVotos.text = "${selectVotos(nombre.toString())} votos"
        val resultInicial = "%.2f".format((votosIniciales.toDouble() / porcentajeInicial)*100)
        textViewPorcentage.text = "$resultInicial %"
        //textViewPorcentage.text = calcularPorcentaje().toString()

        textViewParejaVotar.text = bundle!!.getString("Nombre")

        buttonVotar.setOnClickListener{

            if(!leerUser(user))
            {
                val votosPareja = selectVotos(nombre.toString())
                var votosAntiguos = votosPareja.toInt() + 1


                val pareja = Pareja(
                    Integer.parseInt(idPareja),
                    nombrePareja,
                    Integer.parseInt((votosAntiguos.toString()))
                )
                db.updateCouple(pareja)

                val votosFinales = selectVotos(nombre.toString())
                val procentajeFinal = calcularPorcentaje()

                //val divicion = procentajeFinal / votosFinales.toDouble()
                val result = "%.2f".format((votosFinales.toDouble() / procentajeFinal)*100)
                textViewPorcentage.text = "$result %"

                textViewVotos.text = "${selectVotos(nombre.toString())} votos"

                Toast.makeText(applicationContext, "$user ha votado", Toast.LENGTH_SHORT).show()

                val usuario = Usuario(
                    null,
                    user!!,
                    1
                )
                db.updateUser(usuario)
            }else
            {
                Toast.makeText(applicationContext, "El usuario $user ya ha votado", Toast.LENGTH_SHORT).show()
            }



        }
    }

    private fun selectNombre(nombre: String): String
    {
        var nno = ""
        val lst = db.allPerson
        lst.forEach{
            if(it.name == nombre)
            {
                nno = it.name.toString()
            }
        }

        return nno
    }

    private fun selectId(nombre: String): String
    {
        var nno = ""
        val lst = db.allPerson
        lst.forEach{
            if(it.name == nombre)
            {
                nno = it.id.toString()
            }
        }

        return nno
    }

    private fun selectVotos(nombre: String): String
    {
        var nno = ""
        val lst = db.allPerson
        lst.forEach{
            if(it.name == nombre)
            {
                nno = it.votos.toString()
            }
        }

        return nno
    }

    private fun calcularPorcentaje(): Double{
        var res = 0.0
        val lst = db.allPerson

        lst.forEach{
            res += it.votos
        }

        return res
    }

    private fun leerUser(user: String?): Boolean{
        var res = false
        val lst = db.allUser

        lst.forEach{
            if(it.user == user)
            {
                if(it.votaciones > 0)
                {
                    res = true
                }
            }
        }

        return res
    }

}
